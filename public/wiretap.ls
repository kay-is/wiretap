class Wiretap

  app: void
  url: void
  dev: no

  hearbeat-rate = 10sec

  # private static
  send = !->
    script = document.createElement \script
    script.src = it
    child = document.head.appendChild script
    document.head.removeChild child

  # private static
  start-heartbeat = (wiretap)!->
    heartbeat = !-> wiretap.tap event:\heartbeat
    setInterval heartbeat, hearbeat-rate * 1000

  # constructor
  ({@app, @url, @dev = off, heartbeat = on})->
    start-heartbeat this if heartbeat

  # public
  tap: ({event, dev = off})!~>
    request = "//#{@url}/#{@app}/#{event}"
    request += '?dev' if dev or @dev
    send request