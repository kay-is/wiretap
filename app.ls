require! <[./config.json fs crypto http colors connect connect-route]>

const ACCEPTED = 202
const FORBIDDEN = 403

unless fs.existsSync config.data-dir
  console.log 'Creating data directory ' + config.data-dir.green
  fs.mkdirSync config.data-dir
  fs.mkdirSync config.data-dir + '/dev/'

console.log 'Allowed client domain ' + config.allowed-domain.green
allowed-domain = new RegExp "^#{config.allowed-domain}:"

get-hour-timestamp = ->
  date = new Date
  date = new Date date.getFullYear!, date.getMonth!, date.getDay!, date.getHours!
  date.getTime!

get-client-id = ->
  ip = it.headers['x-forwarded-for'] or it.connection.remoteAddress or
    it.socket.remoteAddress or it.connection.socket.remoteAddress
  user-agent = it.headers['user-agent']
  hash = crypto.createHash \sha1
  hash.update ip
  hash.update user-agent
  hash.digest \base64

wiretap = (request, response)!->
  if allowed-domain.test request.headers.host
    response.statusCode = ACCEPTED

    file-path = config.data-dir + '/'
    file-path += 'dev/' if request.query.dev?
    file-path += request.params.app
    file-path += '/' + get-hour-timestamp! + \.csv

    client-id = get-client-id request

    entry = (new Date).get-time! + \, + client-id + \, + request.params.event + \\n

    error <- fs.appendFile file-path, entry
    if error
      console.error 'Log directory for app ' + request.params.app.red + ' not found!'
  else
    response.statusCode = FORBIDDEN
  response.end!

app = connect!
  .use connect.static \public
  .use connect.query!
  .use connect-route (router)!-> router.get '/wiretap/:app/:event', wiretap

host = config.host + \: + config.port
console.log 'Starting server on ' + host.green
server = http.create-server app
server.listen config.port, config.host